using UnityEngine;

namespace Ludare
{
	public class CoinControllerBehaviour : MonoBehaviour
	{
		[SerializeField]
		private OnTrigger2DEventsBehaviour _onTrigger2DEvents;
		public OnTrigger2DEventsBehaviour OnTrigger2DEvents => _onTrigger2DEvents;

		private void Start()
		{
			// Add one to the counter 
			MainSceneContextBehaviour.INSTANCE.CoinCounter.MaxCount++;
			
			_onTrigger2DEvents.OnTrigger2DEnter += (script, collider) => {
				MainSceneContextBehaviour.INSTANCE.CoinCounter.AddOne();
				Destroy(this.gameObject);
			};
		}
	}
}
