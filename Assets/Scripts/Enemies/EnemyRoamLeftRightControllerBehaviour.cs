using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// Class that encompasses all components for controlling an enemy that 
	/// goes left and right on a platform.
	/// 
	/// Everytime the enemy "detects" (by using the GroundChecker object) 
	/// that it has reached the edge, the enemy will simply change 
	/// walk direction.
	/// 
	/// The idea is to have the unit constantly move (by using the 
	/// UnitLRMover component) by the normalized variable "_xWalkDirection".
	/// This variable can be either 1 (moving to the right) or -1 (moving
	/// to the left), and will only change when the edge detection event from the 
	/// GroundChecker is raised.
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 17 2021
	/// </summary>
	[RequireComponent(typeof(Rigidbody2D))]
	[RequireComponent(typeof(SpriteRenderer))]
	public class EnemyRoamLeftRightControllerBehaviour : MonoBehaviour
	{
		// ---------------------------------------------------PUBLIC PROPERTIES
		[SerializeField]
		private LR_Type _startingDirection;

		// ---------------------------------------PUBLIC COMPONENT DEPENDENCIES
		[Header("--- COMPONENT DEPENDENCIES ---")]
		[SerializeField]
		private UnitLRMoverBehaviour _unitLRMover;
		public UnitLRMoverBehaviour UnitLRMover => _unitLRMover;

		[SerializeField]
		private UnitGroundCheckerBehaviour _unitGroundChecker;
		public UnitGroundCheckerBehaviour UnitGroundChecker => _unitGroundChecker;

		[SerializeField]
		private UnitEnergyChangerBehaviour _unitEnergyChanger;
		public UnitEnergyChangerBehaviour UnitEnergyChanger => _unitEnergyChanger;

		[Header("Used to register player headstomps (check layer of trigger events object)")]
		[SerializeField]
		private OnTrigger2DEventsBehaviour _onTrigger2DEvents;
		public OnTrigger2DEventsBehaviour OnTrigger2DEvents => _onTrigger2DEvents;

		[Header("\"Jump\" inertia percent for player when headstomping enemy")]
		[SerializeField]
		private float _headstompPlayerJumpPercent;
		public float HeadstompPlayerJumpPercent => _headstompPlayerJumpPercent;

		// --------------------------------------PRIVATE COMPONENT DEPENDENCIES

		/// <summary>
		/// Used for flipping the sprite dependning on where the character 
		/// is pointing at (Left or Right)
		/// </summary>
		private SpriteRenderer _spriteRenderer;
		public SpriteRenderer SpriteRenderer => _spriteRenderer;


		// ---------------------------------------------------PRIVATE VARIABLES
		/// <summary>
		/// Represents the direction in which this unit will walk. Will change
		/// value everytime this unit either detects it has reached the edge 
		/// of the ground or when bumping against another enemy.
		/// 
		/// Can be either 1 (moving to the right) or -1 (moving
		/// to the left), and will only change when the edge detection event 
		/// from the GroundChecker is raised.
		/// </summary>
		private float _xWalkDirection;

		// -------------------------------------------------------UNITY METHODS

		private void Awake()
		{
			// Cache private required components
			_spriteRenderer = GetComponent<SpriteRenderer>();

			// Initialize walk direction (x axis xalue):
			// Right :	---> 1F
			// Left:	<--- -1F
			_xWalkDirection = _startingDirection == LR_Type.Right ? 1F : -1F;
		}

		private void Start()
		{
			// Register events:

			// Sprite flip left or right
			_unitLRMover.OnLookDirectionChange += (script, lookLeftOrRight) =>
			{
				_spriteRenderer.flipX = (lookLeftOrRight == LR_Type.Left) ? false : true;
			};

			// Revert direction when the unit detects reaching the edge of the
			// ground.
			// "status" defines which of the both edges 
			_unitGroundChecker.OnLR_GroundStatusChanged += (script, status) =>
			{
				// When any edge of the grouncChecker doesn't detect a ground
				// (far left or far right edges), the "OnLR_GroundStatusChanged"
				// will fire, toggling the direction in which the unit will walk
				if (status != LR_GroundStatus.Both && status != LR_GroundStatus.None)
					_ToggleDirection();
			};

			//				PLAYER STOMP ON ENEMY EVENT (DEAL DAMAGE)

			// Player stomp on the head of the enemy event registration
			// (With Trigger events)
			_onTrigger2DEvents.OnTrigger2DEnter += (script, collider) =>
			{
				// Reduces one value to the enemy's energy
				// (see UNIT IS KILLED (ENERGY IS ZERO) below)
				_unitEnergyChanger.ChangeEnergy(-1);
			};

			//				UNIT IS KILLED (ENERGY IS ZERO)
			_unitEnergyChanger.OnZeroEnergy += script =>
			{

				// Add a bit of jump action to the player
				MainSceneContextBehaviour.INSTANCE.PlayerController.UnitJumper.Jump(_headstompPlayerJumpPercent);

				// Destroy this enemy
				Destroy(this.gameObject);
			};
		}

		private void Update()
		{
			// Constantly move (_xWalkDirection will always be either
			// 1:	to walk right
			// -1:	to walk left)
			// and will only change when the GroundChecker fires the
			// "detected edge" event (search inside the "Start" unity method
			// for "OnLR_GroundStatusChanged" even registration)
			UnitLRMover.Move(_xWalkDirection);


		}

		private void OnCollisionEnter2D(Collision2D collision)
		{
			// Enemy colliding enemy: Change direction

			// The layer this object is in (Enemy) only collides against
			// Ground or Player. We just want to toggle the direction when
			// bumping against another enemy
			if (string.Equals(collision.gameObject.tag, "Enemy") == true)
				_ToggleDirection();
		}

		// -----------------------------------------------------PRIVATE METHODS

		/// <summary>
		/// Toggles the walk direction of this enemy. _xWalkDirection value 
		/// is used inside the Update method
		/// </summary>
		private void _ToggleDirection()
		{
			_xWalkDirection *= -1;
		}
	}
}