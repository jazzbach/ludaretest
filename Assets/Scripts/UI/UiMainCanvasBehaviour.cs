using UnityEngine;
using TMPro;

namespace Ludare
{
	public class UiMainCanvasBehaviour : MonoBehaviour
	{
		[SerializeField]
		private TMP_Text _uiTextCoin;
		public TMP_Text UiTextCoin => _uiTextCoin;
	}
}