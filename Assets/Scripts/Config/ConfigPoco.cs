namespace Ludare
{
	/// <summary>
	/// Plain old c# object (poco) representing the config data used to
	/// change the config of the game.
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 15 2021
	/// 
	/// </summary>
	[System.Serializable]
	public class ConfigPoco
	{
		public bool willUseConfig;
		public float jumpForce;

		public void Initialize() {
			willUseConfig = false;
			jumpForce = 480F;
		}
	}
}