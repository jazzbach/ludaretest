using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// Class that represents a sort of "Service Locator" where all the objects
	/// in the scene are concentrated for ease of retrieval.
	/// 
	/// This is also the point where the config file will be loaded and used
	/// to change the values of the objects. This is mainly for changing
	/// various aspects of the game like the jump force of the player,
	/// the left and right movement force of the player, among other config
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 17 2021
	/// 
	/// </summary>
	public class MainSceneContextBehaviour : MonoBehaviour
	{
		// ----------------------------------------------------STATIC REFERENCE

		/// <summary>
		/// Instance of this class as static to make it available anywhere
		/// (it must be assigned on Awake and nulled OnDestroy)
		/// </summary>
		public static MainSceneContextBehaviour INSTANCE { get; private set; } = null;

		// ----------------------------------------------------UNITY PROPERTIES
		[Header("Camera dependencies")]
		[SerializeField]
		private Camera _mainCamera;
		public Camera MainCamera => _mainCamera;

		[SerializeField]
		private ObjectPositionFollowerBehaviour _cameraToPlayerPositionMatcher;
		public ObjectPositionFollowerBehaviour CameraToPlayerPositionMatcher => _cameraToPlayerPositionMatcher;

		[Header("Player dependencies")]
		[SerializeField]
		private PlayerControllerBehaviour _playerController;
		public PlayerControllerBehaviour PlayerController => _playerController;

		[SerializeField]
		private Transform _playerStartingPoint;
		public Transform PlayerStartingPoint => _playerStartingPoint;

		[SerializeField]
		private bool _doesPlayerStartAtStartingPoint;
		public bool DoesPlayerStartAtStartingPoint => _doesPlayerStartAtStartingPoint;

		[SerializeField]
		private ObjectPositionLerpReseterBehaviour _playerPositionLerpReseter;
		public ObjectPositionLerpReseterBehaviour PlayerPositionLerpReseter => _playerPositionLerpReseter;

		[Header("Scene loader (for loading a scene with a fader)")]
		[SerializeField]
		private SceneLoaderBehaviour _sceneLoader;
		public SceneLoaderBehaviour SceneLoader => _sceneLoader;

		[Header("Coin counter")]
		[SerializeField]
		private CounterBehaviour _coinCounter;
		public CounterBehaviour CoinCounter => _coinCounter;

		[Header("UI Main Canvas")]
		[SerializeField]
		private UiMainCanvasBehaviour _uiMainCanvas;
		public UiMainCanvasBehaviour UiMainCanvas => _uiMainCanvas;

		[Header("Player Won Animator")]
		[SerializeField]
		private Animator _playerWonAnimator;
		public Animator PlayerWonAnimator => _playerWonAnimator;

		// ----------------------------------------------------PUBLIC VARIABLES

		[HideInInspector]
		public ConfigPoco configFile;

		// -------------------------------------------------------UNITY METHODS

		private void Awake()
		{
			// Make this instance available through the application
			INSTANCE = this;
			_LoadConfigFile();
		}

		private void Start()
		{
			_UseConfigFileToConfigureObjects();
			_CheckPlayerStartingPosition();

			//			EVENT REGISTRARION

			//					COINS
			_coinCounter.OnMaxCountChange += script => _UI_SetCoinText();
			_coinCounter.OnCurrentCountChange += script => _UI_SetCoinText();
		}

		private void OnDestroy()
		{
			// Make this instance unavailable through the application
			INSTANCE = null;
		}

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Resets the player position in a lerp fashion (will move the
		/// player to the player start position)
		/// </summary>
		public void ResetPlayerPosition()
		{
			// Disable the collider and the player controller input
			_playerController.Rigidbody2D.isKinematic = true;
			_playerController.Collider.enabled = false;
			_playerController.IsInputDisabled = true;
			_playerController.SetSpriteAlpha(0.5F);

			// Move the player to the start position in a lerp manner
			// (At the end of the lerp, the given action (lambda expresison)
			// will be executed)
			_playerPositionLerpReseter.Lerp(
				_playerController.transform,
				_playerStartingPoint,
				() =>
				{

					LoadScene("MAP_MainScene");
					//// Enable the collider and the player controller input
					//_playerController.Rigidbody2D.isKinematic = false;
					//_playerController.Collider.enabled = true;
					//_playerController.IsInputDisabled = false;

					// Max out player's energy
					//_playerController.UnitEnergyChanger.MaxOutCurrentEnergy();
				}
			);
		}

		/// <summary>
		/// Convenience method that will load a scene with the fader
		/// Is defined here so that it can be used at the player end of level
		/// animator as an animation event
		/// </summary>
		/// <param name="sceneName">Scene name to be loaded</param>
		public void LoadScene(string sceneName) {
			_sceneLoader.LoadScene(sceneName);
		}

		/// <summary>
		/// Plays the end scene animation
		/// </summary>
		public void TriggerPlayerWonAnimation() {
			// Disable player input
			_playerController.IsInputDisabled = true;

			// Fire the end animation event
			_playerWonAnimator.SetTrigger("won");
		}

		// -----------------------------------------------------PRIVATE METHODS

		/// <summary>
		/// Loads the config file from disk. If it doesn't exist, it'll 
		/// create a new one
		/// </summary>
		/// <returns></returns>
		private bool _LoadConfigFile()
		{
			bool t_wasLoaded =
				IOConfigLoaders.LoadConfigFile(out this.configFile);

			return t_wasLoaded;
		}

		/// <summary>
		/// Once the configuration file has been loaded, use it to change
		/// the values of in-game objects
		/// </summary>
		private void _UseConfigFileToConfigureObjects()
		{

			// If the config file is null or if the config file exists but
			// the config won't be taken into consideration, return
			if (configFile == null || configFile.willUseConfig == false) return;

			_playerController.UnitJumper.JumpForce = configFile.jumpForce;
		}

		/// <summary>
		/// Checks if the player has to be positioned in the given position
		/// or not
		/// </summary>
		private void _CheckPlayerStartingPosition()
		{
			if (_doesPlayerStartAtStartingPoint == false) return;

			_playerController.gameObject.transform.position =
				_playerStartingPoint.position;
		}

		/// <summary>
		/// Sets the text of the UI (the coin counting)
		/// </summary>
		private void _UI_SetCoinText()
		{
			_uiMainCanvas.UiTextCoin.text = $"{_coinCounter.CurrentCount} / {_coinCounter.MaxCount}";
		}
	}
}