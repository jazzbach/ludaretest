using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// Contains the logic for finishing the level, which is calling the 
	/// animation from the scene context
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 20 2021
	/// 
	/// </summary>
	public class EndFlagControllerBehaviour : MonoBehaviour
	{
		[SerializeField]
		private OnTrigger2DEventsBehaviour _triggerEvents;

		private void Start()
		{
			// When the player touches the trigger, it'll fire the
			// animation
			_triggerEvents.OnTrigger2DEnter += (script, collider) =>
			{
				MainSceneContextBehaviour.INSTANCE.TriggerPlayerWonAnimation();
			};
		}
	}
}