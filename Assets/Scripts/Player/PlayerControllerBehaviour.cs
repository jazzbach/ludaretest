using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// Class that provides the core components of the player behaviour and 
	/// its dependencies
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 14 2021
	/// 
	/// </summary>
	[RequireComponent(typeof(Rigidbody2D))]
	[RequireComponent(typeof(SpriteRenderer))]
	public class PlayerControllerBehaviour : MonoBehaviour
	{
		// ---------------------------------------------------PUBLIC PROPERTIES
		[Header("Is input disabled ?")]
		[SerializeField]
		private bool _isInputDisabled;
		public bool IsInputDisabled {
			get { return _isInputDisabled; }
			set { 
				_isInputDisabled = value;
				_rigidbody2D.velocity = Vector2.zero;
			}
		}

		[Header("Max amount of air jumps")]
		[SerializeField]
		private byte _maxAirJumps = 1;
		public byte MaxAirJumps
		{
			get { return _maxAirJumps; }
			set { _maxAirJumps = value; }
		}

		// ---------------------------------------PUBLIC COMPONENT DEPENDENCIES
		[Header("--- COMPONENT DEPENDENCIES ---")]
		[SerializeField]
		private UnitLRMoverBehaviour _unitLRMover;
		public UnitLRMoverBehaviour UnitLRMover => _unitLRMover;

		[SerializeField]
		private UnitJumperBehaviour _unitJumper;
		public UnitJumperBehaviour UnitJumper => _unitJumper;

		[SerializeField]
		private UnitGroundCheckerBehaviour _unitGroundChecker;
		public UnitGroundCheckerBehaviour UnitGroundChecker => _unitGroundChecker;

		[SerializeField]
		private UnitEnergyChangerBehaviour _unitEnergyChanger;
		public UnitEnergyChangerBehaviour UnitEnergyChanger => _unitEnergyChanger;

		// --------------------------------------PRIVATE COMPONENT DEPENDENCIES

		/// <summary>
		/// Used mainly to have it available to whoever is interested in it
		/// (for example when the player "dies", it'll be brought back to the 
		/// start of the level. The object in charge of that can deactivate
		/// the collider to do it)
		/// </summary>
		private Rigidbody2D _rigidbody2D;
		public Rigidbody2D Rigidbody2D => _rigidbody2D;

		/// <summary>
		/// Used mainly to have it available to whoever is interested in it
		/// (for example when the player "dies", it'll be brought back to the 
		/// start of the level. The object in charge of that can deactivate
		/// the collider to do it)
		/// </summary>
		private BoxCollider2D _collider;
		public BoxCollider2D Collider => _collider;

		/// <summary>
		/// Used for flipping the sprite dependning on where the character 
		/// is pointing at (Left or Right)
		/// </summary>
		private SpriteRenderer _spriteRenderer;
		public SpriteRenderer SpriteRenderer => _spriteRenderer;

		// ---------------------------------------------------PRIVATE VARIABLES

		/// <summary>
		/// Variable that holds the amount of jumps available.
		/// 
		/// The way air jumping works is that, whenever the jump button 
		/// is pressed (and, if this value isn't zero), this value will be 
		/// substracted one unit.
		/// 
		/// Now, in Start, an event for _unitGroundChecker.OnGroundStatusChanged
		/// is registered, reseting the amount of air jumps available. Please
		/// check the comments in the listener implementation as to how it's
		/// reset
		/// </summary>
		private byte _jumpsAvailable;


		// -------------------------------------------------------UNITY METHODS

		private void Awake()
		{
			// Cache private required components
			_rigidbody2D = GetComponent<Rigidbody2D>();
			_collider = GetComponent<BoxCollider2D>();
			_spriteRenderer = GetComponent<SpriteRenderer>();

			// Reset air jump count
			_ResetAirJumpCount();
		}

		private void Start()
		{

			//					EVENT REGISTRATION


			// Sprite flip left or right
			_unitLRMover.OnLookDirectionChange += (script, lookLeftOrRight) =>
			{
				_spriteRenderer.flipX = (lookLeftOrRight == LR_Type.Left) ? true : false;
			};

			// Double jump reset (when changing states in ground checker)
			// This event will be raised whenever the player either leaves or lands
			// on the ground. Whatever the case, the number of air jumps will be reset.
			// 
			// This case considers when both, the player presses jump, on the ground
			// and when the player falls off of a cliff (the player didn't press jump
			// but is in the air)
			_unitGroundChecker.OnGroundStatusChanged += (script, isGrounded) =>
			{

				_ResetAirJumpCount();
			};

			// Player death (Damage dealt in "OnCollisionEnter2D()")
			_unitEnergyChanger.OnZeroEnergy += script => {
				MainSceneContextBehaviour.INSTANCE.ResetPlayerPosition();
			};

			//				INITIALIZATION

			// Max out current player energy
			_unitEnergyChanger.MaxOutCurrentEnergy();
		}

		private void Update()
		{
			// Check the input (if it's not disabled)
			_CheckInput();
		}

		/// <summary>
		/// Check collision to deal damage to player
		/// </summary>
		/// <param name="collision"></param>
		private void OnCollisionEnter2D(Collision2D collision)
		{
			// Player colliding with enemy: RIP player :(
			if (string.Equals(collision.gameObject.tag, "Enemy"))
			{
				// Deal damage to the player
				_unitEnergyChanger.ChangeEnergy(-1);
			}
		}

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Sets the alpha color of the sprite renderer
		/// </summary>
		/// <param name="alphaValue">alpha value for the sprite renderer</param>
		public void SetSpriteAlpha(float alphaValue) {
			Color t_color = _spriteRenderer.color;
			t_color.a = alphaValue;
			_spriteRenderer.color = t_color;
		}

		// -----------------------------------------------------PRIVATE METHODS

		/// <summary>
		/// Resets the air jump count
		/// </summary>
		private void _ResetAirJumpCount()
		{
			_jumpsAvailable = _maxAirJumps;
		}

		private void _CheckInput() {

			if (_isInputDisabled == true) return;

			// Horizontal move check
			UnitLRMover.Move(Input.GetAxisRaw("Horizontal"));

			// Jump move check
			if (Input.GetButtonDown("Jump") && _jumpsAvailable > 0)
			{
				UnitJumper.Jump(1F);
				_jumpsAvailable--;
			}
		}
	}
}