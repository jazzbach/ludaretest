using System;
using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// This component requires a RigidBody2D
	/// 
	/// Class that allows an object with a RigidBody2D to jump
	/// by calling the "Jump" method and applying force to the RigidBody.
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 14 2021
	/// 
	/// </summary>
	[RequireComponent(typeof(Rigidbody2D))]
	public class UnitJumperBehaviour : MonoBehaviour
	{
		// --------------------------------------------------------UNITY FIELDS
		[Header("Jump force (will use Rigidbody to jump)")]
		[SerializeField]
		private float _JumpForce = 200F;
		public float JumpForce
		{
			get { return _JumpForce; }
			set { _JumpForce = value; }
		}

		[Header("Is inertia considered when jumping ?")]
		[SerializeField]
		private bool _isInertiaConsidered;
		public bool IsInerInertiaConsidered
		{
			get { return _isInertiaConsidered; }
			set { _isInertiaConsidered = value; }
		}

		// ---------------------------------------------------CACHED COMPONENTS
		/// <summary>
		/// Used for adding force for jump
		/// </summary>
		private Rigidbody2D _rigidbody2D;

		// --------------------------------------------------------------EVENTS
		/// <summary>
		/// Fired when a jump happens:
		///		arg1: Instance of this class
		/// </summary>
		public Action<UnitJumperBehaviour> OnJump;

		// -------------------------------------------------------UNITY METHODS
		private void Awake()
		{
			// Cache components
			_rigidbody2D = GetComponent<Rigidbody2D>();
		}

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Add force vertically to the RigidBody
		/// 
		/// Note that the value of "axisValue" acts as a multiplier to the 
		/// amount of "_JumpForce" to be applied. 
		/// A value of 1 will make the unit "jump" at exactly the value of 
		/// "_JumpForce". Higher or lower values will affect the amount of
		/// force applied. Negative forces will make the character "jump"
		/// downwards.
		/// 
		/// If "IsInertiaConsidered" is true, the current vertical velocity
		/// will be taken into consideration when jumping. For example, if
		/// the unit is falling and a mid-air jump is executed, the falling 
		/// speed will counter the jump force. However, if "IsInertiaConsidered"
		/// is false, whatever vertical speed the unit has at the time of the 
		/// jump, it won't be considered at all.
		/// 
		/// Rigidbody's mass will be taken into consideration.
		/// 
		/// OnJump event will be fired when calling this method
		/// </summary>
		/// <param name="axisValue"></param>
		public void Jump(float axisValue)
		{
			// Nullify Y velocity only If inertia is not considered
			if(_isInertiaConsidered == false) _rigidbody2D.velocity *= Vector3.right;

			_AddForceY(axisValue * _JumpForce);

			OnJump?.Invoke(this);
		}

		// -----------------------------------------------------PRIVATE METHODS

		/// <summary>
		/// Add force only in the Y component of the Rigidbody
		/// </summary>
		/// <param name="forceY">Amount of force</param>
		private void _AddForceY(float forceY)
		{
			Vector2 t_motionVector = new Vector2(
					_rigidbody2D.velocity.x,    // X (no changes)
					forceY                      // Y
				);
			_rigidbody2D.AddForce(t_motionVector, ForceMode2D.Force);
		}
	}
}