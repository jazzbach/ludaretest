namespace Ludare
{
	/// <summary>
	/// Enumerator representing Left or Right values
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 14 2021
	/// </summary>
	public enum LR_Type
	{
		Left, Right
	}
}