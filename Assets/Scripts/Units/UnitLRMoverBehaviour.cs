using System;
using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// This component requires a RigidBody2D
	/// 
	/// Class that allows an object with a RigidBody2D to move left and right 
	/// by calling the "Move" method and applying velocity to the RigidBody.
	/// 
	/// If a change in direction happens, an event will be fired.
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 14 2021
	/// 
	/// </summary>
	[RequireComponent(typeof(Rigidbody2D))]
	public class UnitLRMoverBehaviour : MonoBehaviour
	{
		// --------------------------------------------------------UNITY FIELDS
		[Header("Horizontal move force (will use Rigidbody to move)")]
		[SerializeField]
		private float _HorizontalMoveForce = 300F;
		public float HorizontalMoveForce
		{
			get { return _HorizontalMoveForce; }
			set { _HorizontalMoveForce = value; }
		}

		// ---------------------------------------------------CACHED COMPONENTS
		/// <summary>
		/// Used for changing player velocity
		/// </summary>
		private Rigidbody2D _rigidbody2D;

		// ---------------------------------------------------PUBLIC PROPERTIES
		/// <summary>
		/// Direction in which the character is currently facing towards
		/// </summary>
		public LR_Type LookDirection { get; private set; }

		// --------------------------------------------------------------EVENTS
		/// <summary>
		/// Fired when the look direction changes:
		///		arg1: Instance of this class
		///		arg2 (LR_Type): Whether the unit is looking left or right
		/// </summary>
		public Action<UnitLRMoverBehaviour, LR_Type> OnLookDirectionChange;

		// -------------------------------------------------------UNITY METHODS
		private void Awake()
		{
			// Cache components
			_rigidbody2D = GetComponent<Rigidbody2D>();
		}

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Moves the unit to the left, if "axisValue" is negative (-X) or
		/// to the right, if "axisValue" is positive (+X).
		/// 
		/// Note that the value of "axisValue" acts as a multiplier to the 
		/// amount of "_HorizontalMoveForce" to be applied. 
		/// A value of 1 (or -1) will move the unit right (or left, respectively).
		/// Higher (or lower) values will "scale" the result.
		/// 
		/// In order to stop the unit from moving, a value of zero must be
		/// sent as argument (axisValue)
		/// 
		/// Rigidbody's mass won't be taken into consideration.
		/// 
		/// When a change in look direction happens, the "OnLookDirectionChange"
		/// event will be fired
		/// </summary>
		/// <param name="axisValue"></param>
		public void Move(float axisValue)
		{
			Vector2 t_motionVector = new Vector2(
				axisValue * _HorizontalMoveForce,	// X
				_rigidbody2D.velocity.y				// Y (no changes)
			);

			_rigidbody2D.velocity = t_motionVector;

			// Check and set the look direction (and possible fire the event
			// if the direction changed)
			_DoCheckLookDirection(t_motionVector.x);
		}

		// -----------------------------------------------------PRIVATE METHODS

		/// <summary>
		/// Checks if a change in direction happened. If so, sets the 
		/// LookDirection value and fire an event
		/// 
		/// </summary>
		/// <param name="axisValue"></param>
		private void _DoCheckLookDirection(float axisValue)
		{
			if (axisValue == 0F) return;

			LR_Type t_lookLeftOrRight = axisValue > 0 ? LR_Type.Right : LR_Type.Left;

			if (t_lookLeftOrRight != LookDirection)
			{
				LookDirection = t_lookLeftOrRight;
				OnLookDirectionChange?.Invoke(this, LookDirection);
			}
		}
	}
}