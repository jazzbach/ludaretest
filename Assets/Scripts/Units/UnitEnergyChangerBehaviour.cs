using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// Class that 
	/// </summary>
	public class UnitEnergyChangerBehaviour : MonoBehaviour
	{
		// ----------------------------------------------------UNITY PROPERTIES

		// Should not be negative
		[Header("Max energy that unit could have")]
		[SerializeField]
		private int _maxEnergy;
		public int MaxEnergy {
			get { return _maxEnergy; }
			set { _maxEnergy = Mathf.Clamp(value, 0, int.MaxValue); }
		}

		[Header("Unit current energy")]
		[SerializeField]
		private int _currentEnergy;
		public int CurrentEnergy
		{
			get { return _currentEnergy; }
			private set { _currentEnergy = value; }
		}

		// --------------------------------------------------------------EVENTS

		// We can add more events here like when gaining or reducing energy

		/// <summary>
		/// 
		/// </summary>
		public Action<UnitEnergyChangerBehaviour> OnZeroEnergy;

		// ----------------------------------------------------- PUBLIC METHODS

		/// <summary>
		/// Maxes out the current energy to whatever max energy value was 
		/// defined in "_maxEnergy"
		/// </summary>
		public void MaxOutCurrentEnergy() {
			_currentEnergy = _maxEnergy;
		}

		/// <summary>
		/// Changes the current energy value to whatever value "posOrNegValue"
		/// has
		/// 
		/// If it's a positive value, the energy will go up that amount
		/// If it's a negative value, the energy will go down that amount
		/// 
		/// When the energy reaches zero, the event "OnZeroEnergy" will be
		/// raised
		/// </summary>
		/// <param name="posOrNegValue"></param>
		public void ChangeEnergy(int posOrNegValue) {
			_currentEnergy += posOrNegValue;

			// If the energy has reached zero or below
			if (_currentEnergy < 1) {
				_currentEnergy = 0;

				OnZeroEnergy?.Invoke(this);
			}
		}
	}
}