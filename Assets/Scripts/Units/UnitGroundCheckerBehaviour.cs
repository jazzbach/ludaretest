using System;
using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// Enum that represents any of the results from checking if the far left
	/// and/or far right rays are hitting the ground
	/// </summary>
	public enum LR_GroundStatus
	{
		None, Left, Right, Both
	}

	/// <summary>
	/// Class that helps detect if a unit is touching the ground or not at 
	/// any given moment. It uses rays to collide against colliders that belong
	/// to a given layerMask.
	/// 
	/// "_raycastStartPoints" is an array of Transforms that represent the 
	/// starting point of each ray. From each transform, a ray of direction 
	/// and length given by "_raycastsVectorDir" will be computed each frame.
	/// 
	/// It's important to define that, at least 3 values must be in this
	/// array. Also, the order in which the transforms must be added is from the
	/// far left, towards the far right.
	/// 
	/// If any of these rays collide with a collider in the given "_layerMask"
	/// value, the unit is considered "grounded".
	/// 
	/// This class offers states and events to signal not just when the unit 
	/// is grounded or not, but also, when the far left and/or the far right
	/// rays are grounded or not. This is helpful when having units that must
	/// detect if they have reached the edge of the ground to perform some
	/// action.
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 14 2021
	/// 
	/// </summary>
	public class UnitGroundCheckerBehaviour : MonoBehaviour
	{
		// ----------------------------------------------------UNITY PROPERTIES
		[Header("Starting points of all downward raycasts (left to right, all in order. Minimum 3)")]
		[SerializeField]
		private Transform[] _raycastStartPoints;

		[Header("Raycasts vector direction (if any raycast collides, unit is grounded)")]
		[SerializeField]
		private Vector3 _raycastsVectorDir;
		public Vector3 RaycastsVectorDir {
			get { return _raycastsVectorDir; }
			set { _raycastsVectorDir = value; }
		}

		[Header("Raycasts color")]
		[SerializeField]
		private Color _raycastColor;

		[Header("\"Ground\" collision layer mask")]
		[SerializeField]
		private LayerMask _layerMask;
		public LayerMask LayerMask {
			get { return _layerMask; }
			set { _layerMask = value; }
		}

		// ---------------------------------------------------PUBLIC PROPERTIES
		/// <summary>
		/// Is the unit currently grounded ?
		/// </summary>
		public bool IsGrounded { get; private set; }

		/// <summary>
		/// Status of the far left and right edges of the ground checker
		/// (if a tip of the left or the right is not touching the ground 
		/// anymore, this value will detect that)
		/// </summary>
		public LR_GroundStatus LR_GroundStatus { get; private set; }

		// --------------------------------------------------------------EVENTS

		/// <summary>
		/// Fired when the unit either completely leaves the ground or 
		/// at least one ray touches it
		///		First arg: This script instance
		///		Second arg (bool): True: Is the unit fully touching the ground or not
		/// </summary>
		public Action<UnitGroundCheckerBehaviour, bool> OnGroundStatusChanged;

		/// <summary>
		/// Fired when the far left and/or right tips of the ground rays
		/// (the rays) change ground status (touching ground or not)
		///		First arg: This script instance
		///		Second arg (LeftRightRayHitStatus): 
		///			Which edges are touching the ground ?(None, Left, Right or Both)
		/// </summary>
		public Action<UnitGroundCheckerBehaviour, LR_GroundStatus> OnLR_GroundStatusChanged;

		// -------------------------------------------------------UNITY METHODS

		/// <summary>
		/// Used to draw the rays in the Unity Inspector
		/// </summary>
		private void OnDrawGizmos()
		{
			foreach (Transform t_raycastPos in _raycastStartPoints)
			{
				Gizmos.color = _raycastColor;
				Gizmos.DrawRay(t_raycastPos.position, _raycastsVectorDir);
			}
		}

		private void Start()
		{
			// Do initial ground checking to set states
			// (Update will cache these values as "previous values" to check
			// for differences between current and previous values. If a change
			// is detected, an event will be fired)

			// Check if the middle rays are touching the ground
			IsGrounded = _CheckIsGrounded_MiddleRays();

			// Check the status of the edge rays (far left and far right rays)
			LR_GroundStatus = _CheckIsGrounded_EdgeRays();
		}

		private void Update()
		{
			DoAllGroundChecking();
		}

		// -----------------------------------------------------PRIVATE METHODS

		/// <summary>
		/// Performs all the ground checking logic.
		/// </summary>
		private void DoAllGroundChecking()
		{
			//			Previous frame data

			// Temp cache all current state values as "previous frame values"
			// (to check against current values.
			// If there are changes, events will be raised)
			bool _wasGrounded = IsGrounded;
			LR_GroundStatus _previous_LR_GroundStatus = LR_GroundStatus;

			//			Current frame data (temp)

			// temp cache if middle rays are grounded (temp value instead of
			// assigning value to "_wasGrounded" as we still need to check for
			// the edge rays (far left and far right rays (LR)))
			bool t_areMiddleRaysGrounded = _CheckIsGrounded_MiddleRays();
			// get the status of the edge rays (far left and far right rays)
			LR_GroundStatus = _CheckIsGrounded_EdgeRays();

			//			Far left and far right ray (ground) event firing

			// If previous frame LR grounding data is different from current
			// frame, fire an event (a change in status was detected)
			if (_previous_LR_GroundStatus != LR_GroundStatus)
			{
				OnLR_GroundStatusChanged?.Invoke(this, LR_GroundStatus);
			}

			//			Unit grounded (checking all rays) event firing

			// For the unit to be grounded, any ray should be hitting the ground
			// (any middle rays and any LR rays (far left or far right rays))
			IsGrounded = t_areMiddleRaysGrounded || (LR_GroundStatus != LR_GroundStatus.None);

			// If previous frame "Grounding" data is different from current frame
			// fire an event (a change in status was detected)
			if (_wasGrounded != IsGrounded)
			{
				OnGroundStatusChanged?.Invoke(this, IsGrounded);
			}
		}

		/// <summary>
		/// Checks if the middle rays (excluding the far left and far right 
		/// rays) are touching the ground or not
		/// </summary>
		/// <returns>True: If the middle rays are touching the ground. 
		/// False otherwise</returns>
		private bool _CheckIsGrounded_MiddleRays()
		{
			// Check only the middle raycasts (exclude the far left and far right)
			// This is to separate the raycast check of the far left and far right
			// raycasts to check if the unit has reached either side
			for (int i = 1, len = _raycastStartPoints.Length - 1; i < len; i++)
			{
				RaycastHit2D t_hit = _DoRaycast(_raycastStartPoints[i]);

				// If at least one ray hit something, it's grounded
				if (t_hit.collider != null)
					return true;
				else
					continue;
			}

			// No ray was hitting anything. Bummer :(
			return false;
		}

		/// <summary>
		/// Checks only the far left and far right rays for ground collision.
		/// </summary>
		/// <returns>
		///		None: If neither ray is grounded
		///		Left: If only the far left ray is grounded
		///		Right: If only the far right ray is grounded
		///		Both: If both, far left and far right rays are grounded
		///	</returns>
		private LR_GroundStatus _CheckIsGrounded_EdgeRays()
		{

			// Far left raycast collision:
			RaycastHit2D t_hitLeft = _DoRaycast(_raycastStartPoints[0]);

			// Far right raycast collision:
			RaycastHit2D t_hitRight =
				_DoRaycast(_raycastStartPoints[_raycastStartPoints.Length - 1]);

			// To check which result will be returned from the enum, the enum
			// values are arranged as follows:
			//		None	(value = 0)
			//		Left	(value = 1)
			//		Right	(value = 2)
			//		Both	(value = 3)
			// For each raycast hit (left and right) we check if a collision
			// occurred and if so, we simply sum it's corresponding enum value
			// If no collision happened, 0 will be added. This is in order to 
			// avoid "if", "else" clauses
			int t_intReturnVal = 0;
			t_intReturnVal += (t_hitLeft.collider == null) ? 0 : 1;
			t_intReturnVal += (t_hitRight.collider == null) ? 0 : 2;
			return (LR_GroundStatus)t_intReturnVal;
		}

		/// <summary>
		/// Convenience method for doing a raycast. Uses a transform where it's
		/// position will be taken.
		/// 
		/// All the rest parameters for the raycast will be taken from the
		/// respective class variables (direction, distance and layer mask)
		/// </summary>
		/// <param name="transformPosition">The transform of the origin of 
		/// the raycast. Using Transform instead of position for 
		/// convenience purposes. 
		/// Its position will be taken internally</param>
		/// <returns>The raycast hit information</returns>
		private RaycastHit2D _DoRaycast(Transform transformPosition)
		{
			return Physics2D.Raycast(
				transformPosition.position,     // Position of the ray
				_raycastsVectorDir,             // Direction
				_raycastsVectorDir.magnitude,   // Distance
				_layerMask);                    // Collision layer mask
		}
	}
}