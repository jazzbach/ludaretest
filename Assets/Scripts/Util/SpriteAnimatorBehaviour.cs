using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Ludare
{
	/// <summary>
	/// Helper class that allows to perform sprite animations 
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 19 2021
	/// 
	/// </summary>
	public class SpriteAnimatorBehaviour : MonoBehaviour
	{
		// -----------------------------------------------------UNITY PROPERTIES

		[Header("The sprite renderer component where animations will show")]
		[SerializeField] private SpriteRenderer _spriteRenderer;
		public SpriteRenderer SpriteRenderer => _spriteRenderer;
		
		[Header("Sprites added here, NOT in SpriteRenderer component above !!!")]
		[SerializeField]
		private Sprite[] _sprites;
		public Sprite[] Sprites => _sprites;

		[Header("Time per sprite change (in seconds)")]
		[SerializeField]
		private float _timePerChange;
		public float TimePerChange => _timePerChange;

		// ----------------------------------------------------PRIVATE VARIABLES

		/// <summary>
		/// Caches the current index (the current sprite shown) 
		/// from the _sprites array 
		/// </summary>
		private int _currentIndex;

		/// <summary>
		/// The maximum index value (the length of the "_sprites" array)
		/// </summary>
		private int _maxIndex;

		/// <summary>
		/// Referecne to the coroutine that will do all the animation
		/// (in order to stop in when disabling this object)
		/// </summary>
		private Coroutine _animationCoroutine;

		/// <summary>
		/// Cached object uised in the coroutine in order to prevent creating
		/// new objects of this type when yielding
		/// </summary>
		private WaitForEndOfFrame _waitForEndOfFrame = new WaitForEndOfFrame();

		// --------------------------------------------------------UNITY METHODS

		private void Awake()
		{
			_maxIndex = Sprites.Length;
		}

		private void OnEnable()
		{
			_animationCoroutine = StartCoroutine(_PerformAnimation());
		}

		private void OnDisable()
		{
			StopCoroutine(_animationCoroutine);
		}

		// -------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Moves to the next sprite in the _sprites array. 
		/// 
		/// If the current sprite is the last, it wraps around to the beginning 
		/// of the array
		/// </summary>
		public void Next()
		{
			// Add one to _currentIndex before doing the modulo operation with
			// _maxIndex (The modulo operation os to "wrap around" the value
			// back to zero in case _currentIndex is equal or bigger than
			// _maxIndex)
			_currentIndex = ++_currentIndex % _maxIndex;
			_spriteRenderer.sprite = Sprites[_currentIndex];
		}

		// ------------------------------------------------------PRIVATE METHODS

		/// <summary>
		/// 
		/// Coroutine that will do the actual animation</summary>
		/// <returns></returns>
		private IEnumerator _PerformAnimation()
		{
			_Reset();
			float t_tempCounter = 0F;

			while (true)
			{
				if (t_tempCounter > _timePerChange)
				{
					t_tempCounter = 0F;
					Next();
				}

				t_tempCounter += Time.deltaTime;
				yield return _waitForEndOfFrame;
			}
		}

		/// <summary>
		/// Resets the current index value to "zero"
		/// </summary>
		private void _Reset()
		{
			_currentIndex = -1;
			Next();
		}
	}
}