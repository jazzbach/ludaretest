using System;
using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// Moves one object to another object's position after a period of time
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 17 2021
	/// </summary>
	public class ObjectPositionLerpReseterBehaviour : MonoBehaviour
	{
		// ----------------------------------------------------UNITY PROPERTIES
		[Header("Lerper that will ease out the movement")]
		[SerializeField]
		private AnimCurveLerperBehaviour _lerper;
		public AnimCurveLerperBehaviour Lerper => _lerper;

		// ---------------------------------------------------PRIVATE VARIABLES

		/// <summary>
		/// Variables that will be cahced whenever the "Lerp" method occurs
		/// This is done because the lerping events will be carried out by 
		/// In a different method (as registered events inside Start)
		/// </summary>
		private Transform _startPos;
		private Transform _endPos;
		private Action _whatToDoOnEnd = null;

		private Vector3 _originalStartPos;

		// -------------------------------------------------------UNITY MEHTODS

		private void Start()
		{
			_lerper.OnLerpStay += (script, percent, curveVal) =>
			{
				_startPos.position =
						Vector3.Lerp(
							_originalStartPos,
							_endPos.position,
							curveVal);
			};

			_lerper.OnLerpEnd += script =>
			{
				_whatToDoOnEnd?.Invoke();
			};
		}

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Fires the repositioning of the objects. The first Transform's 
		/// position will be taken as the start position
		/// </summary>
		/// <param name="startPos">The start position of the given transform</param>
		/// <param name="endPos">End position (will use transform.position)</param>
		/// <param name="whatToDoOnEnd">Optional action that can be sent to be 
		/// executed when the reposition ends</param>
		/// <returns></returns>
		public bool Lerp(Transform startPos, Transform endPos, Action whatToDoOnEnd = null)
		{
			if (startPos == null || endPos == null) return false;

			_startPos = startPos;
			_endPos = endPos;
			_whatToDoOnEnd = whatToDoOnEnd;

			_originalStartPos = _startPos.position;

			return _lerper.Lerp();
		}
	}

}