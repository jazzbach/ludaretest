using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Ludare
{
	public class AnimCurveLerperBehaviour : MonoBehaviour
	{
		[SerializeField]
		private AnimationCurve _lerpCurve;
		public AnimationCurve LerpCurve => _lerpCurve;

		[SerializeField]
		private float _lerpTimeSeconds;
		public float LerpTimeSeconds {
			get { return _lerpTimeSeconds; }
			set { _lerpTimeSeconds = value; }
		}

		// --------------------------------------------------------------EVENTS

		/// <summary>
		/// 
		/// </summary>
		public Action<AnimCurveLerperBehaviour> OnLerpStart;

		/// <summary>
		///		1st float: x value (percent of the curve value. Between 0 and 1)
		///		2nd float: curve value at x
		/// </summary>
		public Action<AnimCurveLerperBehaviour, float, float> OnLerpStay;

		public Action<AnimCurveLerperBehaviour> OnLerpEnd;

		// ---------------------------------------------------PRIVATE VARIABLES

		/// <summary>
		/// Used in the coroutine down below (in order to not create new objects
		/// on every yield)
		/// </summary>
		private WaitForEndOfFrame _waitForEndOFFrame = new WaitForEndOfFrame();

		private Coroutine _coLerpReference = null;

		// ------------------------------------------------------PUBLIC METHODS
		public bool Lerp()
		{
			if (_coLerpReference != null) return false;
			_coLerpReference = StartCoroutine(_CoLerp());
			return true;
		}

		// -----------------------------------------------------PRIVATE METHODS
		private IEnumerator _CoLerp()
		{
			float t_currentTime = 0F;

			OnLerpStart?.Invoke(this);
			yield return _waitForEndOFFrame;

			while (t_currentTime < _lerpTimeSeconds)
			{
				
				float t_xValue = t_currentTime / _lerpTimeSeconds;
				OnLerpStay?.Invoke(this, t_xValue, _lerpCurve.Evaluate(t_xValue));
				t_currentTime += Time.deltaTime;
				yield return _waitForEndOFFrame;
			}
			OnLerpStay?.Invoke(this, 1F, _lerpCurve.Evaluate(1F));
			OnLerpEnd?.Invoke(this);

			_coLerpReference = null;
		}
	}
}