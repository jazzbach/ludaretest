using UnityEngine;
using UnityEngine.SceneManagement;

namespace Ludare
{
	/// <summary>
	/// Class that can load a scene with a fading effect
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 19 2021
	/// 
	/// </summary>
	public class SceneLoaderBehaviour : MonoBehaviour
	{
		[Header("Canvas fader (fade in and out ui image). Can be null")]
		[SerializeField]
		private CanvasImageFaderBehaviour _canvasImageFader;
		public CanvasImageFaderBehaviour CanvasImageFader => _canvasImageFader;

		[SerializeField]
		private bool _willStartFadeOut;

		private void Start()
		{
			if(_willStartFadeOut == true) _canvasImageFader?.FadeOut();
		}

		/// <summary>
		/// Loads the scene with the given name.
		/// 
		/// If "_canvasImageFader" is not null, it'll fade out before changing
		/// scene
		/// </summary>
		/// <param name="sceneName">The name of the scene to be loaded</param>
		public void LoadScene(string sceneName) {
			// If the canvas image fader is null. Just load the given scene
			// by sceneName
			if (_canvasImageFader == null) {
				SceneManager.LoadScene(sceneName);
			} else {
				// Fade In and, at the end, load the scene by sceneName
				_canvasImageFader.FadeIn(() => SceneManager.LoadScene(sceneName));
			}
		}
	}
}