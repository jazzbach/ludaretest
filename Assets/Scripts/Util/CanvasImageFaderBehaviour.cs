using System;
using UnityEngine.UI;
using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// Class that provides functionality for fading in and out a UI image
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 19 2021
	/// 
	/// </summary>
	public class CanvasImageFaderBehaviour : MonoBehaviour
	{
		// ---------------------------------------------------PUBLIC PROPERTIES
		[Header("AnimCurveLerper objects for fade in and fade out")]
		[SerializeField]
		private AnimCurveLerperBehaviour _animCurveLerper_FadeIn;
		public AnimCurveLerperBehaviour AnimCurveLerper_FadeIn => _animCurveLerper_FadeIn;

		[SerializeField]
		private AnimCurveLerperBehaviour _animCurveLerper_FadeOut;
		public AnimCurveLerperBehaviour AnimCurveLerper_FadeOut => _animCurveLerper_FadeOut;

		[Header("Ui Image that will fade in - out")]
		[SerializeField]
		private Image _canvasImage;
		public Image CanvasImage => _canvasImage;

		[Header("FADE TIME WILL BE TAKEN FROM HERE (Not from AnimCurveLerper components)")]
		[SerializeField]
		private float _fadeInTime;
		public float FadeInTime
		{
			get { return _fadeInTime; }
			set { _fadeInTime = value; }
		}

		[SerializeField]
		private float _fadeOutTime;
		public float FadeOutTime
		{
			get { return _fadeOutTime; }
			set { _fadeOutTime = value; }
		}

		// ---------------------------------------------------PRIVATE VARIABLES

		/// <summary>
		/// These are action references that can be sent in the FadeIn and FadeOut
		/// methods. Are used in order to call them in a different methods
		/// (registered events in Start ())
		/// </summary>
		private Action _whatToDoOnFadeInEnd = null;
		private Action _whatToDoOnFadeOutEnd = null;

		// --------------------------------------------------------------EVENTS
		
		public Action<CanvasImageFaderBehaviour> OnFadeInStart;
		public Action<CanvasImageFaderBehaviour> OnFadeInEnd;

		public Action<CanvasImageFaderBehaviour> OnFadeOutStart;
		public Action<CanvasImageFaderBehaviour> OnFadeOutEnd;

		// -------------------------------------------------------UNITY METHODS

		private void Start()
		{
			//						REGISTER EVENTS

			// FADE IN - DURING
			_animCurveLerper_FadeIn.OnLerpStay += (script, xVal, animVal) =>
				// Simply set the alpha value of the image according to the
				// curve value
				_SetAlphaToCanvasImage(animVal);

			// FADE IN - END
			_animCurveLerper_FadeIn.OnLerpEnd += script =>
			{
				_whatToDoOnFadeInEnd?.Invoke(); // Execute end action (if any)
				OnFadeInEnd?.Invoke(this); // Fire event
			};


			// FADE OUT - DURING
			_animCurveLerper_FadeOut.OnLerpStay += (script, xVal, animVal) =>
				// Simply set the alpha value of the image according to the
				// curve value
				_SetAlphaToCanvasImage(animVal);

			// FADE OUT - END
			_animCurveLerper_FadeOut.OnLerpEnd += script =>
			{
				_canvasImage.gameObject.SetActive(false);
				_whatToDoOnFadeOutEnd?.Invoke(); // Execute end action (if any)
				OnFadeOutEnd?.Invoke(this); // Fire event
			};
		}

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Fades in the "_canvasImage" (from transparent to solid) in
		/// "_fadeIn" seconds.
		/// 
		/// An action can be sent as an argument that will be executed when
		/// the fade ends
		/// </summary>
		/// <param name="whatToDoOnFadeInEnd"></param>
		public void FadeIn(Action whatToDoOnFadeInEnd = null)
		{
			// Set the time duration
			_CheckZeroValue(ref _fadeInTime);
			_animCurveLerper_FadeIn.LerpTimeSeconds = _fadeInTime;

			// Action to be executed at the end of the fade (if any)
			_whatToDoOnFadeInEnd = whatToDoOnFadeInEnd;

			// Enable the image
			_canvasImage.gameObject.SetActive(true);

			// Invoke the event
			OnFadeInStart?.Invoke(this);

			// Start the lerp (will fire event registered inside Start()
			// in "FADE IN - DURING")
			_animCurveLerper_FadeIn.Lerp(); 
		}

		public void FadeOut(Action whatToDoOnFadeOutEnd = null)
		{
			// Set the time duration
			_CheckZeroValue(ref _fadeOutTime);
			_animCurveLerper_FadeOut.LerpTimeSeconds = _fadeOutTime;

			// Action to be executed at the end of the fade (if any)
			_whatToDoOnFadeOutEnd = whatToDoOnFadeOutEnd;

			// Enable the image and set the alpha to 1
			_canvasImage.gameObject.SetActive(true);
			_SetAlphaToCanvasImage(1F);

			// Invoke the event
			OnFadeOutStart?.Invoke(this);

			// Start the lerp (will fire event registered inside Start()
			// in "FADE OUT - DURING")
			_animCurveLerper_FadeOut.Lerp();
		}

		// -----------------------------------------------------PRIVATE METHODS
		/// <summary>
		/// Checks the given ref value if it's less than zero. If it is
		/// zero will be assigned
		/// </summary>
		/// <param name="valueToCheck"></param>
		private void _CheckZeroValue(ref float valueToCheck)
		{
			if (valueToCheck < 0) valueToCheck = 0;
		}

		/// <summary>
		/// Sets the alpha value of the given image to the given value
		/// </summary>
		/// <param name="image"></param>
		/// <param name="alphaValue"></param>
		private void _SetAlphaToCanvasImage(float alphaValue)
		{
			Color t_color = _canvasImage.color;
			t_color.a = alphaValue;
			_canvasImage.color = t_color;
		}
	}
}