using System;
using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// Class that helps counting and fires events when the counting is maxed 
	/// out or changed
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 20 2021
	/// 
	/// </summary>
	public class CounterBehaviour : MonoBehaviour
	{
		/// <summary>
		/// Max count 
		/// </summary>
		private int _maxCount;
		public int MaxCount
		{
			get { return _maxCount; }
			set
			{
				_maxCount = Mathf.Clamp(value, 0, int.MaxValue);
				CurrentCount = Mathf.Clamp(CurrentCount, 0, _maxCount);
				OnMaxCountChange?.Invoke(this);
			}
		}

		/// <summary>
		/// Current count value
		/// </summary>
		private int _currentCount;
		public int CurrentCount
		{
			get { return _currentCount; }
			set
			{
				_currentCount = Mathf.Clamp(value, 0, _maxCount);
				_CheckEventsIfMaxedOut();
			}
		}

		// --------------------------------------------------------------EVENTS
		public Action<CounterBehaviour> OnMaxCountChange;
		public Action<CounterBehaviour> OnCurrentCountChange;
		public Action<CounterBehaviour> OnCurrentCountMaxed;

		// ------------------------------------------------------PUBLIC METHODS

		/// <summary>
		/// Adds one to the current count
		/// </summary>
		public void AddOne()
		{
			_currentCount++;

			_currentCount = Mathf.Clamp(_currentCount, 0, _maxCount);

			// Check 
			_CheckEventsIfMaxedOut();
		}

		// -----------------------------------------------------PRIVATE METHODS

		/// <summary>
		/// Checks if current count value is maxed out and fire events
		/// </summary>
		private void _CheckEventsIfMaxedOut()
		{
			if (_currentCount == _maxCount) OnCurrentCountMaxed?.Invoke(this);
			OnCurrentCountChange?.Invoke(this);
		}
	}
}