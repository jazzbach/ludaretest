using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// Class that helps set the same position of one object to another
	/// with the option of movement easing, meaning the movement can be
	/// soft or immediate.
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 17 2021
	/// 
	/// </summary>
	public class ObjectPositionFollowerBehaviour : MonoBehaviour
	{
		[Header("The object to be followed")]
		[SerializeField]
		private Transform _targetTransform;

		[Header("The object that will match _targetTransform's position")]
		[SerializeField]
		private Transform _followerTransform;

		[Header("Follow specific values from the _targetTransform")]
		[SerializeField]
		private bool _followX;

		[SerializeField]
		private bool _followY;

		[SerializeField]
		private bool _followZ;

		[Header("Movement easing (0: no easing | 1: soft easing)")]
		[SerializeField]
		[Range(0F, 0.9999F)]
		private float _easing;

		private void Update()
		{
			// Generate a vector with the new position values
			Vector3 t_newPosition = new Vector3(
				_followX ? Mathf.Lerp(_targetTransform.position.x, _followerTransform.position.x, _easing) : _followerTransform.position.x,
				_followY ? Mathf.Lerp(_targetTransform.position.y, _followerTransform.position.y, _easing) : _followerTransform.position.y,
				_followZ ? Mathf.Lerp(_targetTransform.position.z, _followerTransform.position.z, _easing) : _followerTransform.position.z
			);

			_followerTransform.position = t_newPosition;
		}
	}
}
