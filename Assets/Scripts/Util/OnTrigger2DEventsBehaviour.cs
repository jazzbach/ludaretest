using System;
using UnityEngine;

namespace Ludare
{
	/// <summary>
	/// Convenience class that allows to subscribe to Trigger Events
	/// 
	/// Author: Bruno Arturo Costa Hernandez
	/// chilljazzbach@gmail.com
	/// Oct 17 2021
	/// 
	/// </summary>
	[RequireComponent(typeof(BoxCollider2D))] // Required just to draw collider gizmo
	public class OnTrigger2DEventsBehaviour : MonoBehaviour
	{
		// --------------------------------------------------------------EVENTS
		public Action<OnTrigger2DEventsBehaviour, Collider2D> OnTrigger2DEnter;
		public Action<OnTrigger2DEventsBehaviour, Collider2D> OnTrigger2DStay;
		public Action<OnTrigger2DEventsBehaviour, Collider2D> OnTrigger2DExit;

		// -------------------------------------------------------UNITY METHODS

		private void OnTriggerEnter2D(Collider2D collision)
		{
			OnTrigger2DEnter?.Invoke(this, collision);
		}

		private void OnTriggerStay2D(Collider2D collision)
		{
			OnTrigger2DStay?.Invoke(this, collision);
		}

		private void OnTriggerExit2D(Collider2D collision)
		{
			OnTrigger2DExit?.Invoke(this, collision);
		}
	}
}

