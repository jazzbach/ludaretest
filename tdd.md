# Ludare Unity Developer Exercise
# Version 1.0
# Created on Oct 20 2021
# Bruno Arturo Costa Hernandez
---

# Project Summary

Create a Unity project, targeting Windows, for a prototype for a 2D platformer. The goals are to test gameplay, try and test technical approaches and to foresee possible technical problems.

Strive for a fun game feel, even though this is a prototype. A good reference can be the game [Super Mario Bros for NES](https://www.youtube.com/watch?v=Dp7fVUfj8oI).

# Technical Summary

The prototype must be developed with Unity 2020.3.20f1 and all the code must be authored by the developer. Third-party code, projects, templates or similar assets are not allowed. The exceptions are Unity-owned packages like Cinemachine, New Input System, TextMesh Pro, Tilemaps, etc. that may be considered as part of the core game engine.

Proxy art assets can be used as long as they are not copyrighted and must be attributed with their link, their distribution license and any additional relevant information (see [Files Consideration](#-files-consideration)).

The Universal Render Pipeline for 2D must be used for the project.

A GIT repo must be created using any free online version control service (Github, Gitlab, Bitbucket, etc) to store the Unity project in. The repo must contain the corresponding instructions on how to build the app. However, no build should be uploaded.

The app should be able to load a configuration file when executed to change gameplay values like "player jump height" and so on. This config file will be used by an external [Tuning Tool](#-tuning-tool).

# Software

The prototype must be built using 2020.3.20f1 (LTS).

The project can be developed using any operating system, as long as its developed with the Unity version described above. However, the build must run on Windows platforms only. 

# Files Consideration

## Art Files
- Art files can be either .jpg, .png or .tga.
- Non POT textures are accepted.
- Create a "licenses.txt" file in the project root folder with all corresponding attributions.

## Audio Files
- Audio files can be .mp3, .wav or .ogg, although the latter is strongly suggested.

# Naming Conventions

## Code Naming Conventions

The code should follow general C# naming conventions (CamelCase and curly braces on new lines) and should be saved in a folder called "Scripts" inside the "Assets" folder. Code subfolders should be created relative to their functionality. For example, a folder called "Player" should have all the code related to the player.

All classes should belong to the following namespace: **Ludare**.

All code should be properly documented, except for code where its functionality is already defined in the variable, method or event name and that suffices its understanding.

A basic naming template example is given below:
```sh
namespace Ludare // Namespace
{
    /// <summary>
    /// The summary of the class (CamelCase)
    /// </summary>
    public class MyClassName
    {
        // private variables (fields) should start with underscore (for ease of recognition) and follow CamelCase
        private float _exampleOfVariableName;
        
        // public properties should start with uppercase and follow CamelCase
        public float PropertyName;
        
        // methods should start with uppercase and follow CamelCase
        public void ExampleOfMethodName(void arg1)
        {
            // ...
        }
        
        // Coroutine methods should start with the "Co" prefix
        public IEnumerator CoNameOfCoroutine()
        {
            
        }
    }
    
    // Interfaces should have 
    public interface IAnInterface
    {
        // ...
    }
}
```

## Asset Naming Conventions

The following assets should have the following prefixes (name examples are inside parenthesis):

- Prefabs: BP (BP_ScoreManager).
- Textures: TEX (TEX_Background).
- Sprite Textures: SPR (SPR_Enemy).
- Animations and Animators: ANI (ANI_Player).
- Materials: MAT (MAT_Rock).
- Scenes: MAP (MAP_End).

Assets should be organized in folders relative to their functionality. For example, All textures should be saved in a folder called "Textures".

# Game Input Requirements

The player must have the option to play with either a keyboard or a joystick. The mappings are as follows:

- move left/right: Left/right arrows [keyboard] or move left/right the left joystick [joystick].
- Jump: spacebar [keyboard] or any button (called "jump button" below) [joystick].
- Double jump: spacebar twice [keyboard] or jump button twice [joystick].

# Gameplay requirements

Create one finished level that fulfills the following requirements:

- Player should be able to double jump.
- Collectable objects (e.g., coins) picked-up on touch.
- One environmental hazard type that kills the player on touch.
- One moving enemy type that kills the player on touch. The player can kill this enemy by jumping onto its head.
- An object (e.g., a flagpole) placed at the end of each level that triggers the level-win-condition on touch by the player.
- If a player dies, their “ghost” floats back to the starting point and then the level resets.
- UI elements as appropriate.

# Game Metrics

- The **basic unit** for every visual element must be 1 x 1 Unity Units (called UU from now on).
- All **gameplay sprites** should have their "pixels per unit" value set to either their width or height max value. This is to make sure that sprites will be represented within 1 x 1 UU.
- The **player collider**'s width and height must not be bigger than 1 UU. However, it's width and height may vary independently as needed but cannot exceed 1UU (for example, the collider can be an irregular rectangle or a capsule collider). This is also true for other **gameplay colliders** (for enemies, collectables and so on).
- Platform colliders may span more than 1 UU (and could have slight adjustments if needed), as long as they are always built in steps of 1 UU (a platform could be of size 1 x 5 for example).
- Pivot points for platforms should be on the upper, left corner of the platform. This could vary depending on the platform type or other circumstances. However, the pivot point should never be left at the center of the platform.
- Pivot points for the player and enemies should be located in the bottom-center of the unit.

- Jump height for player: more than 2 but less than 3 Unity Units (for both, single and double jump).

# General Architecture

## Scenes

One scene with the main gameplay will suffice, as long as it contains the main gameplay loop and it can be reset when winning or losing.

## GameObject hierarchy

GameObjects should be organized by type in the hierarchy. The parent object's only purpose is to encapsulate these objects. Therefore, their position and rotation must be zero and their scale must be one.

Here's an example of a hierarchy organization:
```sh
Context // Has the Context MonoBehaviourScript (explained below)
    Camera
    Light
WorldObjects
    Coins
        Coin1
        Coin2
        ...
    Platforms
        Platform1
        Platform2
        ...
```

## Prefabs

All prefabs should be saved inside a folder called "Prefabs" and should be organized in subfolders describing their functionality.

## Physics Layers

The following physics layers can be defined for interaction:

```sh
                            player      ground      enemy       triggerByPlayer
    triggerByPlayer     |     X     |           |           |     
    enemy               |     X     |     X     |     X     |-----------
    ground              |     X     |           |-----------|-----------
    player              |           |-----------|-----------|-----------
```

## Context Class

A context class encapsulates all the objects scattered throughout the scene that depend from each other and are needed for gameplay logic. It's a way to provide services and dependencies to all classes.

For example, the UI may be refreshed when a coin is grabbed, but these two objects should not depend on each other. The Context in this case will serve as the "glue" between these two objects.

The Context class should have a static reference to itself that can be called from any other class.

This class will also load the **ludareConfig.json** file and set its values to the appropriate objects accordingly.

## Reusable MonoBehaviour Classes

These classes are MonoBehaviour components that can be reused by other GameObjects in order to reuse their functionality. These should be as event-based as possible.

Through the explanation, the word "Units" will be used to refer to a gameplay object, it could be the player, an enemy, a coin, etc.

All the names of the following classes are just suggestions:

### - Unit Left/Right Mover
- Allows the GameObject to move left or right by changing the velocity x component of the RigidBody2D. 
- Will also provide information to where the object is staring at (Left or Right). 
- Fire event for when the GameObject staring direction changes (may be used to flip the character's sprite left or right).

### - Unit Jumper
- Allows the GameObject to be launched to the air by adding vertical force to the RigidBody2D.
- Fires event for when the GameObject jumps.

### - Unit Ground Checker
- Checks for if the unit is touching the ground or is airborne.
- Should check for when a unit reaches a platform corner (may be used by enemies to turn around when reaching a platform's edge).
- Fires event for when the unit is immediately grounded or immediately in the air.
- Fires event for when the unit has reached a platform's edge.

### - Unit Energy
- Has the count of the current unit's energy (life points).
- Can add or subtract energy.
- Fires event for when the energy reaches Zero.

### - Trigger 2D Events
- Encapsulates functionality for when trigger collisions happen (used for coins, hazards or for when player reaches the goal, for example).
- Provides events for when a trigger enter, stay and exit.

### - Object Position Follower
- Provides services for matching one object's position to another on every frame (can be immediate match or lerping).
- Could be used for the camera following the player.

### - Object Position Resetter
- Provides services for moving one object from one position to another after a period of time.
- Could be used for teletransporting the player's "ghost" to the start of the level when killed.
- May provide events for when the repositioning is happening and when it ends.

### - Scene Loader
- Provides services for loading a specific scene and potentially adding a fade in-out effect to it.
- May be used for when the player is killed or when the player reaches the goal to load the scene again.

## Player

A player object has a "Controller" MonoBehaviour that does the following:
- Encapsulates all its component references (Collider, SpriteRenderer, RigidBody, Unit components, etc) to make them publicly available.
- Uses all the encapsulated components to define its gameplay logic.
- Will be dealt damage whenever an enemy or a hazard touches it (collider or trigger).

Player components:

- PlayerController.
- Unit Left/Right Mover.
- Unit Jumper.
- Unit Ground Checker.
- Unit Energy.

This GameObject should be saved as a prefab.

**Layer : player**.
**Tag : player**.

## Enemy

A enemy object has a "Controller" MonoBehaviour that does the following:
- Encapsulates all its component references (Collider, SpriteRenderer, RigidBody, Unit components, etc) to make them publicly available.
- Moves left and right on a platform, changing direction when either reaching the platform's edge or when bumping into an enemy).

Enemy components:

- EnemyController.
- Unit Left/Right Mover.
- Unit Ground Checker.
- Unit Energy.

This GameObject should be saved as a prefab.

**Layer : enemy**.
**Tag : enemy**.

## Platform 

A platform A hazard may not have a Controller class. It just needs to have a Collider.

**Layer : ground**.
**Tag : ground**.

# Coin

A coin object has a "Controller" MonoBehaviour that does the following:
- Encapsulates all its component references.
- Disappears when touched by the player and send a message to the ui to update the counting by using the Context class).

Coin components:

- Trigger 2D Events.

This GameObject should be saved as a prefab.

**Layer : triggerByPlayer**.
**Tag : triggerByPlayer**.

# Hazard

A hazard may not have a Controller class. It just needs to have a Collider.

**Layer : enemy, ground (enemies can walk on them)**.
**Tag : enemy**.

# End Flag

An end flag object has a "Controller" MonoBehaviour that does the following:
- Encapsulates all its component references.
- When touched by the player, fires the win animation by using the Context class.

End flag components:

- Trigger 2D Events.

This GameObject should be saved as a prefab.

**Layer : triggerByPlayer**.
**Tag : triggerByPlayer**.

## User Interface

The user interface will display the coins that have been obtained so far by the player and a reference to it should be added to the Context class.

The position of the coin indicator will be on the top-left corner of the screen.

## Configuration file

The prototype itself must use a config file (json file) that gives the user the chance to override certain in-game values without having to change them inside Unity. This must also work in a build. These values can be changed when the prototype is not running so that the next time its launches, they will be loaded, affecting the corresponding gameplay.

This config file can be changed by the user manually of using another automated tool (like explained in the [Tuning Tool](#-tuning-tool) section).

The name of the configuration file must be **ludareConfig.json** and must reside in the project's root folder.

If the file doesn't exist when the prototype is launched, it must be automatically generated.

The basic structure of the file is as follows:
```sh
{
    "willUseConfig": false,
    "jumpForce": 480.0
}
```

"jumpForce" is used in this example by means of showing the name and value structure of these variables. More variables can be added to the config file. However, they must be programmed to be loaded within Unity.

if "willUseConfig" = true, the configuration file must be loaded and its values must affect the corresponding gameplay elements. If its false, the config file will be completely skipped by Unity, using the internal default values.

The configuration file should be loaded within the Context class as it encapsulates the important references of the objects in the scene.

The classes that will be used for all the config file IO as well as the Object/Json parsing should be separated into their functionality:

- Class to Reading/Writing strings from/to files.
- Class for Object/JSON(string) parsing.

All these classes should be placed inside a folder called "IO", inside the "Scripts" folder.

---
# Tuning Tool
---

# Summary

The tuning tool must be a standalone Windows application that will have to open an external resource (a spread sheet) and read values from it that represent configuration values that can be used to tune gameplay. The tool will have to write those values to the **ludareConfig.json** so that the prototype could use them when being launched.

The spreadsheet must be a Microsoft Excel document (.xlsx).

The very first column (Column 1) must have all the variable names defined in the "ludareConfig.json" file, except for
the "willUseConfig" value. The names of the values must match lower and upper case names from the config file.

The second column (Column 2) will have the values for the corresponding variables on the left.

All the data must be in the first worksheet.

# Technical Summary

The tool must be a standalone application that can be written in any language. 

---
# Overall Task Estimates
---
|Task|                          Time (hours)|
|---|---|
|Analysis|                      5|
|Prototype Tool Research|       3|
|Prototype Implementation|      16|
|Prototype Testing|             2|
|Prototype Finetunning|         4|
|Tuning Tool Research|          3|
|Tuning Tool Implementation|    7|
|Tuning Tool Testing|           2|
|Tuning Tool Finetunning|       1|
|**Total**| **43**
