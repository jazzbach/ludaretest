# Ludare Unity Developer exercise
# Bruno Arturo Costa Hernandez
# Oct 20 2021
---

# Unity version
2020.3.20f1

# How to build the prototype ?
- Clone the project from https://gitlab.com/jazzbach/ludaretest.git
- Open the project in Unity
- Go to "File -> Build Settings" and click the "Build" button
- Select the folder in which to save the build and click the "Select Folder" button
- The build process will start. When it finishes, a folder will pop up showing the build files.

# How to play the prototype ?
- Open the folder where the build is located and double click on the "LudareTest.exe" icon and the game will launch

# Prototype controls
The prototype can be played with either a keyboard or a xBox controller. The mappings are as follows:

- move left/right: Left/right arrows [keyboard] or move left/right the left joystick [joystick].
- Jump: spacebar [keyboard] or "A" button [joystick].
- Double jump: spacebar twice [keyboard] or "A" button twice [joystick].
- 
# Technical Design Document (TDD) link
https://drive.google.com/file/d/1NrWfgtF-U0piFFdPkA8gRiqqPhB_CxZq/view?usp=sharing

# Licenses

### Asset Store

Free Platform Game Assets
https://assetstore.unity.com/packages/2d/environments/free-platform-game-assets-85838

### External

Enemy sprite:
https://bevouliin.com/marshmallow-with-earphone-sprites/

Background:
Tamara Ramsay http://vectorgurl.com/